﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CategoryElement.cs" company="VipSoft.com.cn">
//    Author:Chen,Jun
//        QQ:47262947
//     Email:chenjun@VipSoft.com.cn
//    Create:24-Mar-2013
// </copyright>
namespace VipSoft.CMS.Core.Enum
{
    /// <summary>
    /// 界面上的HTML元素，用来判断是否显示在界面上
    /// </summary>
    public enum CategoryElement
    {
        Content=1,
        List
    }
}
